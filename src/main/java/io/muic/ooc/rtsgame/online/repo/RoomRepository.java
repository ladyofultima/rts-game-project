package io.muic.ooc.rtsgame.online.repo;

import io.muic.ooc.rtsgame.online.model.Room;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository("roomRepository")
public interface RoomRepository extends CrudRepository<Room, Long>{
    Room findRoomByName(String name);
}
