package io.muic.ooc.rtsgame.online.repo;

import io.muic.ooc.rtsgame.online.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository("userRepository")
public interface UserRepository extends CrudRepository<User, Long>{
    User findByUsername(String username);

}
