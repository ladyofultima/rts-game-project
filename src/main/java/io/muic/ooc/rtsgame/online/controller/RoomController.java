package io.muic.ooc.rtsgame.online.controller;

import io.muic.ooc.rtsgame.online.model.Room;
import io.muic.ooc.rtsgame.online.model.User;
import io.muic.ooc.rtsgame.online.service.RoomService;
import io.muic.ooc.rtsgame.online.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.*;

@RestController
@RequestMapping("/api/v1/room")
public class RoomController {
    @Autowired
    UserService userService;

    @Autowired
    RoomService roomService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public Set<Room> roomIndex(Principal principal){
        return roomService.listRoom();
    }

    @RequestMapping(value = {"/show/{id}"}, method = RequestMethod.GET)
    public Room showRoom(@PathVariable(value="id") int id, HttpServletResponse response){
        Room room = roomService.findById(id);
        if(room == null){
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return null;
        }else {
            return roomService.findById(id);
        }

    }

    @RequestMapping(value = {"/create"}, method = RequestMethod.POST)
    public Map<String, String> createRoom(Principal principal, HttpServletResponse response,
        @RequestParam(value="roomName") String name,
                                          @RequestParam(value = "password", required = false) String password){
        Room room = new Room();
        room.setName(name);
        room.setPassword(name);
        User user = userService.findByUsername(principal.getName());
        if(user.getRoom() == null){
            if (StringUtils.isBlank(room.getName())){
                return new HashMap<String, String>(){{

                    put("status", "fail");
                    put("message", "Room name is blank ");
                }};
            }
            roomService.createRoom(user, room);
            return new HashMap<String, String>(){{
                put("status", "success");
                put("message", "ok");
            }};
        }else {
            return new HashMap<String, String>(){{
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                put("status", "fail");
                put("message", "the user is already in a room exit the room first" );
            }};
        }

    }


    @RequestMapping(value = {"/join/{id}"}, method = RequestMethod.GET)
    public Map<String, Object> joinRoom(@PathVariable(value = "id") int id, Principal principal,
                                        HttpServletResponse response){
        Room room = roomService.findById(id);
        User user = userService.findByUsername(principal.getName());
        if(room == null){
            return new HashMap<String, Object>(){{
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                put("status", "fail");
                put("message", "the room does not exist");
            }};
        }else if(room.getUsers().size() == 2){
            return new HashMap<String, Object>(){{
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                put("status", "fail");
                put("message", "the room is full");
            }};
        }else {
            roomService.join(user, room);
            return new HashMap<String, Object>(){{
                put("status", "success");
                put("message", "ok");
                put("room", room);
            }};
        }
    }

    @RequestMapping(value = {"/leave"}, method = RequestMethod.GET)
    public Map<String, String> leaveRoom(Principal principal, HttpServletResponse response){
        User user = userService.findByUsername(principal.getName());
        Room room = user.getRoom();

        if(user.getRoom() != null){
            roomService.leaveRoom(user);
            return new HashMap<String, String>(){{
                put("status", "success");
                put("message", "ok");
            }};
        }else {
            return new HashMap<String, String>(){{
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                put("status", "fail");
                put("message", "the user have no room to leave");
            }};
        }

    }

    @RequestMapping(value = {"/toggle_ready"}, method = RequestMethod.GET)
    public Map<String, String> toggleReady(Principal principal, HttpServletResponse response){
        User user = userService.findByUsername(principal.getName());
        Room room = user.getRoom();
        if(roomService.toggleReady(user, room)){
            return new HashMap<String, String>(){{
                put("status", "success");
                put("message", "ok");
                put("toggleStatus", user.isReady() ? "true" : "false");
            }};
        }else{
            return new HashMap<String, String>(){{
                put("status", "fail");
                put("message", "user is not in a room");
            }};
        }
    }

}
