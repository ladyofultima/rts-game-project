package io.muic.ooc.rtsgame.online.controller;

import io.muic.ooc.rtsgame.online.model.User;
import io.muic.ooc.rtsgame.online.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class LoginController {

    @Autowired
    private UserService userService;

    /**
     * register the user into the system
     * @param user
     * @return {status, message}
     */

    @RequestMapping(value = {"/register"}, method = RequestMethod.POST)
    public Map<String, Object> createNewUser(HttpServletResponse response, @RequestParam(value = "username") String username,
                                             @RequestParam(value = "password") String password, @RequestParam(value = "name") String name){
        User userExists = userService.findByUsername(username);
        if(userExists != null){
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return new HashMap<String, Object>(){{
                put("status", "fail");
                put("message", "the user already exists");
            }};
        }
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setName(name);
        userService.saveUser(user);
        return new HashMap<String, Object>(){{
            put("status", "success");
            put("message", "successfully register the user");
        }};
    }

    @RequestMapping(value = "/whoami", method = RequestMethod.GET)
    public Map<String, Object> queryUser(Principal principal, HttpServletResponse response){
        try{
            User user = userService.findByUsername(principal.getName());
            return new HashMap<String, Object>(){{
               put("status", "success");
               put("user", user);
            }};
        }catch (Exception ex){
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return new HashMap<String, Object>(){{
                put("status", "fail");
                put("message", "unauthorized");
            }};
        }
    }
}
