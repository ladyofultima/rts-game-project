package io.muic.ooc.rtsgame.online.service;

import io.muic.ooc.rtsgame.online.model.Role;
import io.muic.ooc.rtsgame.online.model.User;
import io.muic.ooc.rtsgame.online.repo.RoleRepository;
import io.muic.ooc.rtsgame.online.repo.UserRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Service("userService")
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    String testString = "start";


    @Scheduled(fixedRate=1000)
    public void updateTest(){
        testString = RandomStringUtils.randomAlphabetic(5);
    }

    
    public String getTest() {
            return testString;
        }

    
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    
    public void saveUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setActive(1);
        Role userRole = roleRepository.findByRole("PLAYER");
        user.setRoles(new HashSet<>(Arrays.asList(userRole)));
        userRepository.save(user);

    }

    public void setInGame(User user, boolean status){
        user.setInGame(status);
        userRepository.save(user);
    }

    
    public void deleteUser(User user) {
        User u = userRepository.findByUsername(user.getUsername());
        userRepository.delete(u);
    }

    
    public void editName(User user, String newName) {
        User u = userRepository.findByUsername(user.getUsername());
        u.setName(newName);
        userRepository.save(u);

    }

    
    public Set<User> listUsers() {
        Set<User> users = new HashSet<>();
        for(User u : userRepository.findAll()){
            users.add(u);
        }
        return users;
    }

    

}
