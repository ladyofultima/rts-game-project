package io.muic.ooc.rtsgame.online.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.muic.ooc.rtsgame.working.model.Map;
import io.muic.ooc.rtsgame.working.model.Player;
import org.hibernate.annotations.*;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Table(name = "room")
public class Room {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column(name="name", unique = true)
    @NotEmpty(message = "must provide a room name")
    private String name;

    @OneToMany
    @JoinColumn(name = "room_users")
    @JsonManagedReference
    private Set<User> users;

    @Column(name="password")
    @JsonIgnore
    private String password;


    @OneToMany
    @NotFound(action=NotFoundAction.IGNORE)
    @JsonIgnore
    private Set<Player> players;


    @OneToOne
    @JoinColumn(name = "map_id")
    @NotFound(action=NotFoundAction.IGNORE)
    @JsonIgnore
    private Map map;

    public Set<Player> getPlayers() {
        return players;
    }



    public void setPlayers(Set<Player> players) {
        this.players = players;
    }

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
