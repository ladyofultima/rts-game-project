package io.muic.ooc.rtsgame.online.service;

import io.muic.ooc.rtsgame.online.model.Room;
import io.muic.ooc.rtsgame.online.model.User;
import io.muic.ooc.rtsgame.online.repo.RoomRepository;
import io.muic.ooc.rtsgame.online.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Service("roomService")
public class RoomService {
    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private UserRepository userRepository;

    
    public Room findByRoomName(String name) {
        return roomRepository.findRoomByName(name);
    }

    
    public Room findById(long id) {
        return roomRepository.findOne( id);
    }

    
    public void join(User user, Room room) {
        Room currentRoom = roomRepository.findRoomByName(room.getName());
        Set<User> userRoom = currentRoom.getUsers();
        userRoom.add(user);
        currentRoom.setUsers(userRoom);
        user.setRoom(room);
        user.setRoomId(room.getId());
        roomRepository.save(room);
        userRepository.save(user);
    }

    
    public void createRoom(User user, Room room) {
        room.setUsers(new HashSet<>(Arrays.asList(user)));
        user.setRoom(room);
        roomRepository.save(room);
        user.setRoomId(room.getId());
        userRepository.save(user);
    }

    
    public void deleteRoom(Room room) {
        roomRepository.delete(room);
    }

    
    public void changeName(Room room, String newName) {
        Room currentRoom = roomRepository.findRoomByName(room.getName());
        currentRoom.setName(newName);
        roomRepository.save(currentRoom);
    }

    public boolean toggleReady(User user, Room room){
        if(user.getRoomId() > 0 && room.getUsers().contains(user)){
            if(user.isReady()){
                user.setReady(false);
            }else{
                user.setReady(true);
            }
            userRepository.save(user);
            return true;
        }else{
            return false;
        }
    }

    
    public Set<Room> listRoom() {
        Set<Room> rooms = new HashSet<>();
        for(Room room: roomRepository.findAll()){
            rooms.add(room);
        }
        return rooms;
    }

    public void leaveRoom(User user) {
        user.getRoom().getUsers().remove(user);
        Room roomToRemove = null;
        if(user.getRoom().getUsers().isEmpty()){
            roomToRemove = user.getRoom();
        }
        user.setRoom(null);
        user.setReady(false);
        user.setRoomId(-1);
        userRepository.save(user);
        if(roomToRemove != null){
            roomRepository.delete(roomToRemove);
        }
    }
}
