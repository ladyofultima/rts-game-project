package io.muic.ooc.rtsgame.online.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;


@Component
public class RESTAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler{
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        ObjectMapper mapper = new ObjectMapper();
//        response.addHeader("Access-Control-Allow-Origin","http://localhost:5555");
        response.addHeader("Content-Type", "application/json");
//        response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, OPTIONS");
//        response.addHeader("Access-Control-Allow-Credentials", "true");
        response.getWriter().write(mapper.writeValueAsString(
                new HashMap<String, String>(){{
                    put("status", "success");
                    put("message", "login successfully");
                }}
        ));
        clearAuthenticationAttributes(request);
    }
}
