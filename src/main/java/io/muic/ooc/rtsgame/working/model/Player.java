package io.muic.ooc.rtsgame.working.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.muic.ooc.rtsgame.online.model.Room;
import io.muic.ooc.rtsgame.online.model.User;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.annotation.Generated;
import javax.persistence.*;
import java.util.Set;

/**
 * Created by wit on 3/14/2017 AD.
 */
@Entity
public class Player {
    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "player_id")
    private long playerId;

    @OneToOne
    @PrimaryKeyJoinColumn
    @NotFound(action= NotFoundAction.IGNORE)
    private User user;

    @ManyToOne
    @JoinColumn(name = "room_id")
    private Room room;

    @JsonIgnore
    @OneToMany(mappedBy = "player", fetch = FetchType.EAGER)
    Set<PlayerResource> playerResources;

    @Column(name = "is_alive")
    private boolean isAlive;

    @Column(name = "gold_per_second")
    private int goldPerSecond;

    @Column(name = "maximum_unit")
    private int maximumUnit;

    @Column(name = "current_unit")
    private int currentUnit;

    @OneToMany(mappedBy="owner", fetch = FetchType.EAGER)
    @JsonIgnore
    Set<GameEntity> gameEntities;
 
    @OneToMany(mappedBy="owner", fetch = FetchType.EAGER)
    @JsonIgnore
    Set<Grid> grids;

    boolean checkEnd = false;


    public boolean isCheckEnd() {
        return checkEnd;
    }

    public void setCheckEnd(boolean checkEnd) {
        this.checkEnd = checkEnd;
    }

    public void setPlayerId(long playerId) {
        this.playerId = playerId;
    }

    public void setGoldPerSecond(int goldPerSecond) {
        this.goldPerSecond = goldPerSecond;
    }

    public void setMaximumUnit(int maximumUnit) {
        this.maximumUnit = maximumUnit;
    }

    public void setCurrentUnit(int currentUnit) {
        this.currentUnit = currentUnit;
    }

    public int getGoldPerSecond() {
        return goldPerSecond;
    }

    public int getMaximumUnit() {
        return maximumUnit;
    }

    public int getCurrentUnit() {
        return currentUnit;
    }

    public long getPlayerId() {
        return playerId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Set<PlayerResource> getPlayerResources() {
        return playerResources;
    }

    public void setPlayerResources(Set<PlayerResource> playerResources) {
        this.playerResources = playerResources;
    }

    public Set<GameEntity> getGameEntities() {
        return gameEntities;
    }

    public void setGameEntities(Set<GameEntity> gameEntities) {
        this.gameEntities = gameEntities;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    public Set<Grid> getGrids() {
        return grids;
    }

    public void setGrids(Set<Grid> grids) {
        this.grids = grids;
    }

    /**
     * Custom equals function for position objects
     *
     * @param a position object to compare
     * @return boolean
     */
    @Override
    public boolean equals(Object a) {
        if (a instanceof Player) {
            return this.getPlayerId() == ((Player) a).getPlayerId();
        }
        return false;
    }

    /**
     * Custom hashcode for equals function
     */
    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + ((int) this.getPlayerId());
        return result;
    }
}
