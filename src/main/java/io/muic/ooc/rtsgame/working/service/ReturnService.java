package io.muic.ooc.rtsgame.working.service;

import io.muic.ooc.rtsgame.online.model.Room;
import io.muic.ooc.rtsgame.working.model.GameEntity;
import io.muic.ooc.rtsgame.working.model.Grid;
import io.muic.ooc.rtsgame.working.model.Map;
import io.muic.ooc.rtsgame.working.model.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by wit on 3/27/2017 AD.
 */
@Service
public class ReturnService {

    @Autowired
    GridIndexSortService gridIndexSortService;

    public void makeReturn(Player player, Map map, HashMap<String, Object> js){
        for (GameEntity gameEntity: player.getGameEntities()){
            gameEntity.setCoolDownTimeLeft();
        }
        js.put("entities", player.getGameEntities());
        js.put("territory", makeTerritory(player,map));
        js.put("enemies", getEnemyEntities(player,map));
        js.put("gameStatus","onGoing");
    }

    public Set<GameEntity> getEnemyEntities(Player player, Map map){

        Set<GameEntity> gameEntities = player.getGameEntities();
        Set<GameEntity> toReturn = new HashSet<>();
        Set<GameEntity> destroy = new HashSet<>();




        for (GameEntity gameEntity : gameEntities){
            if (gameEntity.getCurrentHealth() <= 0){
                destroy.add(gameEntity);
                continue;
            }
            int x = gameEntity.getPosX();
            int y = gameEntity.getPosY();
            int move = gameEntity.getMoveRange();
            if (move <= 0){
                continue;
            }
            int min_x = x-move;
            int min_y = y-move;
            int max_x = x+move;
            int max_y = y+move;
            for (int i = min_x; i < max_x; i++){
                if (i < 0 || i >= map.getNumberOfColumns()){
                    continue;
                }
                for (int j = min_y; j < max_y; j++){
                    if (j < 0 || j >= map.getNumberOfRows()){
                        continue;
                    }
                    //ensure it's a valid grid pos
                    if (Math.abs(gameEntity.getPosX()-i) + Math.abs(gameEntity.getPosY()-j) <= move){
                        Grid g = map.getGridByGridIndex(i,j);

                        if (g.getGameEntity() != null &&
                                !g.getGameEntity().getGameEntityType().equals("obstacle") &&
                                g.getGameEntity().getOwner().getPlayerId() != player.getPlayerId()){
                            g.getGameEntity().setCoolDownTimeLeft();
                            toReturn.add(g.getGameEntity());
                        }
                    }
                }
            }
        }

        gameEntities.removeAll(destroy);
        player.setGameEntities(gameEntities);

        for (Grid grid: player.getGrids()){
            if (grid.getGameEntity() != null && grid.getOwner() != null && grid.getGameEntity().getOwner().getPlayerId() != player.getPlayerId()){
                if (!toReturn.contains(grid.getGameEntity())){
                    toReturn.add(grid.getGameEntity());
                }
            }
        }

        for (Grid grid: map.getGrids()){
            if (grid.getOwner() != null && grid.getOwner().getPlayerId() != player.getPlayerId()){
                if (grid.getGameEntity() != null && grid.getGameEntity().getGameEntityType().equals("building") && !toReturn.contains(grid.getGameEntity())){
                    toReturn.add(grid.getGameEntity());
                }
            }
        }

//        System.out.println(toReturn);
        return toReturn;
    }


    public int[] makeTerritory(Player player, Map map){
        List<Grid> grids = map.getGrids();
        int[] territory = new int[map.getNumberOfRows() * map.getNumberOfColumns()];

//        System.out.println(grids);
        for (Grid grid : grids){
            if (grid.getGameEntity() != null && grid.getGameEntity().getGameEntityType().equals("obstacle")){
                territory[grid.getGridIndex()] = 200;
            } else if(grid.getOwner() == null){
                territory[grid.getGridIndex()] = 0;
            } else if (grid.getOwner().getPlayerId() == player.getPlayerId()){
                territory[grid.getGridIndex()] = 1;
            }else if(grid.getOwner().getPlayerId() != player.getPlayerId()){
                territory[grid.getGridIndex()] = -1;
            }
        }

        return territory;
    }


    public String makeStateId() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }
}
