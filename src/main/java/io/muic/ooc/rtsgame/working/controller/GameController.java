package io.muic.ooc.rtsgame.working.controller;

import io.muic.ooc.rtsgame.online.model.*;
import io.muic.ooc.rtsgame.online.service.*;
import io.muic.ooc.rtsgame.working.factory.GameEntityFactory;
import io.muic.ooc.rtsgame.working.model.*;
import io.muic.ooc.rtsgame.working.model.Map;
import io.muic.ooc.rtsgame.working.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.security.Principal;
import java.util.*;

/**
 * Created by wit on 3/12/2017 AD.
 */
@RestController
@RequestMapping("/api/v1/game")
public class GameController {

    /**
     * TODO:
     * Make delete game
     * for game creation, make it stable, put HQ into the map for the map creation and do ownership for that
     * for game creation, run through and add "obstacle":[0,1,0,1....,0,0,0]
     * for build, add the gameEntity into the map list of gameEntities to help with the updateMap
     * there's some more for build.
     * for factory, create obstacle
     * for the update post items, make the proper return item, DONE
     * for the update get, make the proper return item and do some checking first
     * test functions
     */

    @Autowired
    UserService userService;
    @Autowired
    RoomService roomService;
    @Autowired
    GameEntityService gameEntityService;
    @Autowired
    PlayerService playerService;
    @Autowired
    GridResourceService gridResourceService;
    @Autowired
    GridService gridService;
    @Autowired
    MapService mapService;
    @Autowired
    PlayerResourceService playerResourceService;
    @Autowired
    ResourceService resourceService;
    @Autowired
    ValidActionService validActionService;
    @Autowired
    GameService gameService;
    @Autowired
    ReturnService returnService;
    @Autowired
    UpdateTerritoryService updateTerritoryService;
    @Autowired
    GridIndexSortService gridIndexSortService;

    @RequestMapping(value = {"/test"})
    public String test(){
        return userService.getTest();
    }

    /**
     * Take in roomId and mapId to create the map
     * get users from the room to create players for the game
     * This thing might need alot of tweaking but once it is stable it will make the game work
     * @param mapName
     * @return
     */
    @RequestMapping(value = {"/create"},method = RequestMethod.POST)
    public synchronized HashMap<String, Object> createGame(
            Principal principal,
            HttpServletResponse response,
            @RequestParam(value = "mapName") String mapName
    ){
        HashMap<String, Object> js = new HashMap<>();
        if(!validActionService.isValidCreate(principal)){
            js.put("create", "failure");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return js;
        }

        try{
            User u =    userService.findByUsername(principal.getName());
            Room room = u.getRoom();
            Map map = mapService.createMap(mapName, room);
            Set<User> users = room.getUsers();
            Set<Player> players = new HashSet<>();
            room.setPlayers(players);
            for (User user : users){
                Player player = playerService.createPlayer(user,room);
                userService.setInGame(user, true);
                players.add(player);
                userService.saveUser(user);
            }

            if (players.size() == 1){
                for(Player player : players){
                    gameService.build(player,map,1,1,"headquarter");
                }
            }else if(players.size() == 2){
                int i = 0;
                for(Player player : players){
                    if(i == 0){
                        gameService.build(player,map,1,1,"headquarter");
                    }else{
                        gameService.build(player,map,22,10,"headquarter");
                    }
                    i++;
                }
            }
            updateTerritoryService.updateTerritory(map);
            gameService.changeMapId(map);
            js.put("stateId", map.getStateId());
            js.put("create", "success");
            map.setReady(true);
            mapService.saveMap(map);
        }catch (Exception ex){
            js.put("create", "failure");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            ex.printStackTrace();
        }
        return js;
    }

    @RequestMapping(value = {"/update/build"}, method = RequestMethod.POST)
    public synchronized HashMap<String, Object> updateBuild(
            Principal principal,
            HttpServletResponse response,
            @RequestParam(value = "entityName", defaultValue = "none") String entityName,
            @RequestParam(value = "location", defaultValue = "none") String location
    ) {
        HashMap<String, Object> js = new HashMap<>();
        if(!validActionService.isValidAction(principal)){
            js.put("actionResult", "failure");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return js;
        }


        String[] loc = location.split(",");
        User user = userService.findByUsername(principal.getName());
        Room room = user.getRoom();
        Map map = mapService.findById(room.getId());
        gridIndexSortService.sortArray(map.getGrids());
        Player player = playerService.findById(user.getId());

        if (validActionService.isValidBuild(player,map,loc,entityName)){
            gameService.build(player,map,loc,entityName);
            js.put("actionResult", "success");
            String stateId = returnService.makeStateId();
            map.setStateId(stateId);
            updateTerritoryService.updateTerritory(map);
            gameService.changeMapId(map);
        }else {
            //do some error shit
            js.put("actionResult", "failure");
        }

        returnService.makeReturn(player,map,js);
        js.put("stateId", map.getStateId());
        return js;
    }

    /**
     * called when a player want to attack
     * split src and dest, get map and player
     * do validation
     * if it pass, attack and set success state
     * if fail, set failure state
     * return the state
     * @param origin
     * @param destination
     * @return
     */
    @RequestMapping(value = {"/update/attack"}, method = RequestMethod.POST)
    public synchronized HashMap<String, Object> updateAttack(
            Principal principal,
            HttpServletResponse response,
            @RequestParam(value = "origin") String origin,
            @RequestParam(value = "destination") String destination
    ) {

        HashMap<String, Object> js = new HashMap<>();
        if(!validActionService.isValidAction(principal)){
            js.put("actionResult", "failure");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return js;
        }

        String[] src = origin.split(",");
        String[] dest = destination.split(",");
        User user = userService.findByUsername(principal.getName());
        Map map = mapService.findById(user.getRoom().getId());
        gridIndexSortService.sortArray(map.getGrids());
        Player player = playerService.findById(user.getId());

        if (validActionService.isValidAttack(player,map,src,dest)){
            js.put("actionResult", "success");
            gameService.attack(map,src,dest);
            map.setStateId(returnService.makeStateId());

        }else {
            js.put("actionResult", "failure");
        }

        player = playerService.findById(user.getId());
        map = mapService.findById(user.getRoom().getId());

        updateTerritoryService.updateTerritory(map);
        gameService.changeMapId(map);
        returnService.makeReturn(player,map,js);
        js.put("stateId", map.getStateId());

        return js;
    }



    /**
     * same step as attack

     * @param origin
     * @param destination
     * @return
     */
    @RequestMapping(value = {"/update/move"}, method = RequestMethod.POST)
    public synchronized HashMap<String, Object> updateMove(
            Principal principal,
            HttpServletResponse response,
            @RequestParam(value = "origin") String origin,
            @RequestParam(value = "destination") String destination
    ) {
        HashMap<String, Object> js = new HashMap<>();
        if(!validActionService.isValidAction(principal)){
            js.put("actionResult", "failure");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return js;
        }

        String[] src = origin.split(",");
        String[] dest = destination.split(",");
        User user = userService.findByUsername(principal.getName());
        Room room = user.getRoom();
        Map map = mapService.findById(room.getId());
        gridIndexSortService.sortArray(map.getGrids());

        Player player = playerService.findById(user.getId());

        if (validActionService.isValidMove(player,map,src,dest)){
            js.put("actionResult", "success");
            gameService.move(map,src,dest);
            map.setStateId(returnService.makeStateId());

        }else {
            js.put("actionResult", "failure");
        }

        player = playerService.findById(user.getId());
        map = mapService.findById(user.getRoom().getId());

        updateTerritoryService.updateTerritory(map);
        gameService.changeMapId(map);

        returnService.makeReturn(player,map,js);
        js.put("stateid", map.getStateId());
        return js;
    }

    @RequestMapping(value = {"/update/merge"}, method = RequestMethod.POST)
    public synchronized HashMap<String, Object> updateMerge(
            Principal principal,
            HttpServletResponse response,
            @RequestParam(value = "origin") String origin,
            @RequestParam(value = "destination") String destination
    ) {
        HashMap<String, Object> js = new HashMap<>();
        if(!validActionService.isValidAction(principal)){
            js.put("actionResult", "failure");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return js;
        }

        String[] src = origin.split(",");
        String[] dest = destination.split(",");
        User user = userService.findByUsername(principal.getName());
        Room room = user.getRoom();
        Map map = mapService.findById(room.getId());
        gridIndexSortService.sortArray(map.getGrids());

        Player player = playerService.findById(user.getId());

        if (validActionService.isValidMerge(player,map,src,dest)){
            gameService.merge(player,map,src,dest);
            js.put("actionResult", "success");
            map.setStateId(returnService.makeStateId());
        }else {
            js.put("actionResult", "failure");
        }

        player = playerService.findById(user.getId());
        map = mapService.findById(user.getRoom().getId());

        updateTerritoryService.updateTerritory(map);
        gameService.changeMapId(map);

        returnService.makeReturn(player,map,js);
        js.put("stateId", map.getStateId());
        return js;
    }




    /**
     * delete the game --> only
     * @return
     */
    public synchronized void endGame(Principal principal){


        User user = userService.findByUsername(principal.getName());
        userService.setInGame(user, false);
        Map map = mapService.findById(user.getRoom().getId());

        Room room = user.getRoom();

        if (map == null){
            return;
        }

        gridIndexSortService.sortArray(map.getGrids());

        Set<Player> players = room.getPlayers();
        List<Grid> grids  = map.getGrids();

        for (Grid grid: grids){
            if (grid.getGameEntity() != null){
                gameEntityService.deleteGameEntity(grid.getGameEntity());
            }
            gridService.deleteGrid(grid);
        }

        for (Player player: players){
            playerService.deletePlayer(player.getPlayerId());
        }

        mapService.deleteMap(map.getMapId());

        roomService.deleteRoom(room);
    }

    /**
     * return just like what other update would return except this one u dont do anything
     */
    @RequestMapping(value = {"/show"}, method = RequestMethod.GET)
    public HashMap<String, Object> showBoard(Principal principal,
                                             HttpServletResponse response) {
        HashMap<String, Object> js = new HashMap<>();
        if(!validActionService.isValidShow(principal)){
            js.put("actionResult", "failure");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return js;
        }

        User user = userService.findByUsername(principal.getName());
        Player player = playerService.findById(user.getId());
        if(gameService.checkGameOver(user.getRoom())){
            playerService.playerCheckEnd(player);
            userService.setInGame(user,false);
            if (player.isAlive()){
                js.put("gameStatus","win");
            }else{
                js.put("gameStatus","lose");
            }

            if(gameService.checkIfAllPlayerCheckEnd(user.getRoom())){
                endGame(principal);
            }
        }else{
            Map map = mapService.findById(user.getRoom().getId());
            gridIndexSortService.sortArray(map.getGrids());
            returnService.makeReturn(player,map,js);
            js.put("stateId", map.getStateId());
        }
        return js;
    }
}
