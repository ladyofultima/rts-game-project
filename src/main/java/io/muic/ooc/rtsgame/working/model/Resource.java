package io.muic.ooc.rtsgame.working.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import java.util.Set;

/**
 * Created by wit on 3/14/2017 AD.
 */
@Entity
public class Resource {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "resource_id")
    private long resourceId;
    @Column(name = "resource_name")
    private String resourceName;

    @OneToMany
    @Cascade({ org.hibernate.annotations.CascadeType.ALL })
    @JoinColumn(name = "grid_resource_id")
    @NotFound(action=NotFoundAction.IGNORE)
    @JsonIgnore
    private Set<GridResource> gridResources;

    @OneToMany(fetch = FetchType.EAGER)
    @JsonIgnore
    Set<PlayerResource> playerResources;

    public long getResourceId() {
        return resourceId;
    }

    public String getResourceName() {
        return resourceName;
    }
}
