package io.muic.ooc.rtsgame.working.repo;

import io.muic.ooc.rtsgame.working.model.GameEntity;
import io.muic.ooc.rtsgame.working.model.Grid;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by wit on 3/20/2017 AD.
 */
@Repository
public interface GameEntityRepository extends CrudRepository<GameEntity, Long>{
    GameEntity findByGrid(Grid grid);
}
