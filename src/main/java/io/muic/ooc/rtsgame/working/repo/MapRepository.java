package io.muic.ooc.rtsgame.working.repo;

import io.muic.ooc.rtsgame.online.model.Room;
import io.muic.ooc.rtsgame.working.model.Map;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by wit on 3/16/2017 AD.
 */
@Repository
public interface MapRepository extends CrudRepository<Map, Long>{
    Map findMapByRoom(Room room);
}
