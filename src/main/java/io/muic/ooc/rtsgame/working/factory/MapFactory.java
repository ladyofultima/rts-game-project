package io.muic.ooc.rtsgame.working.factory;

import io.muic.ooc.rtsgame.online.model.Room;
import io.muic.ooc.rtsgame.working.model.Grid;
import io.muic.ooc.rtsgame.working.model.Map;
import io.muic.ooc.rtsgame.working.service.GridIndexSortService;
import io.muic.ooc.rtsgame.working.service.GridService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by wit on 3/20/2017 AD.
 */
public class MapFactory {
    public static Map createMap(long mapSkeletonId){

        final int COLUMN_LENGTH = 24;
        final int ROW_LENGTH = 12;

        Map map = new Map();
        map.setBuildings(new ArrayList<>());
        map.setNumberOfColumns(COLUMN_LENGTH);
        map.setNumberOfRows(ROW_LENGTH);
        map.setTotalNumberOfGrids(COLUMN_LENGTH * ROW_LENGTH);

        return map;
    }

//    public static List<Grid> initializeGrids(Map map, int columnLength, int rowLength){
//
//        GridService gridService = new GridService();
//
//        List<Grid> gridsList = new ArrayList<>();
//
//        for (int i = 0; i < rowLength; i++){
//            for (int j = 0; j < columnLength; i++){
//                Grid thisGrid = gridService.createGrid(map, j, i);
//                gridsList.add(thisGrid);
//            }
//        }
//
//        return gridsList;
//    }

    private static Map mapOne(){
        return  null;
    }
}
