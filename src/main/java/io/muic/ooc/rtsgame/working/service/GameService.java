package io.muic.ooc.rtsgame.working.service;

import io.muic.ooc.rtsgame.online.model.Room;
import io.muic.ooc.rtsgame.online.service.RoomService;
import io.muic.ooc.rtsgame.online.service.UserService;
import io.muic.ooc.rtsgame.working.factory.GameEntityFactory;
import io.muic.ooc.rtsgame.working.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.Entity;
import java.security.Principal;

/**
 * Created by wit on 3/23/2017 AD.
 */

/**
 * TODO: attack add in the calculation and cooldown, move set the calculation, build change to factory use,
 */
@Service
public class GameService {

    @Autowired
    CalculationService calculationService;

    @Autowired
    UserService userService;
    @Autowired
    RoomService roomService;
    @Autowired
    GameEntityService gameEntityService;
    @Autowired
    PlayerService playerService;
    @Autowired
    GridService gridService;
    @Autowired
    MapService mapService;
    @Autowired
    ReturnService returnService;

    public void changeMapId(Map map){
        map.setStateId(returnService.makeStateId());
        mapService.saveMap(map);
    }

    /**
     * @param src
     * @param dest
     * @param map
     * @return
     */
    public void attack(Map map, String[] src, String[] dest){

        GameEntity attacker = getGridFromMap(map, src).getGameEntity();
        GameEntity defender = getGridFromMap(map, dest).getGameEntity();

        //calculation mode
        //space saved for the calculation
        calculationService.calculateAttack(attacker,defender);


        //cooldown set mode
        calculationService.calculateCooldownTime(attacker);

        //check if the dest died and do necessary operation
        if (defender.getCurrentHealth() <= 0){
            kill(defender, map);
        }else{
            gameEntityService.saveGameEntity(defender);
        }
        gameEntityService.saveGameEntity(attacker);
        mapService.saveMap(map);
    }

    public void kill(GameEntity gameEntity, Map map){
        Grid loc = gameEntity.getGrid();
        Player player = gameEntity.getOwner();

        gameEntity.setGrid(null);


        if (gameEntity.getGameEntityType().equals("unit")){
            player.setCurrentUnit(player.getCurrentUnit()-1);
        }
        if (gameEntity.getGameEntityName().equals("headquarter")){
            player.setAlive(false);
        }
        if (gameEntity.getGameEntityName().equals("barrack")){
            player.setMaximumUnit(player.getMaximumUnit()-5);
        }

        if (gameEntity.getGameEntityType().equals("building")){
            map.getBuildings().remove(gameEntity);
        }

        gameEntityService.deleteGameEntity(gameEntity);
        player.getGameEntities().remove(gameEntity);
        loc.setGameEntity(null);
        gridService.saveGrid(loc);
        playerService.savePlayer(player);
    }

    /**
     * checking is done in the controller
     * @param src
     * @param dest
     * @param map
     */
    public void move(Map map,String[] src, String[] dest){
        Grid srcGrid  = getGridFromMap(map, src);
        Grid destGrid  = getGridFromMap(map, dest);
        GameEntity gameEntity = srcGrid.getGameEntity();

        srcGrid.setGameEntity(null);
        destGrid.setGameEntity(gameEntity);
        gameEntity.setGrid(destGrid);
        gameEntity.setPosX(destGrid.getPosX());
        gameEntity.setPosY(destGrid.getPosY());

        //cooldown set mode
        calculationService.calculateCooldownTime(gameEntity);

        gridService.saveGrid(srcGrid);
        gridService.saveGrid(destGrid);
        gameEntityService.saveGameEntity(gameEntity);
        mapService.saveMap(map);
    }

    /**
     * First: setting game entity player and location
     * Second: set the game entity for the location
     * third: change the status for the player
     * @param player
     * @param map
     * @param loc
     * @param name
     */
    public void build(Player player, Map map, String[] loc, String name){
        Grid locGrid = getGridFromMap(map,loc);


        GameEntity gameEntity = gameEntityService.createGameEntity(name,player,locGrid,Integer.parseInt(loc[0]),Integer.parseInt(loc[1]));

        locGrid.setGameEntity(gameEntity);



        if (!player.getGameEntities().contains(gameEntity)){
            player.getGameEntities().add(gameEntity);
        }
        if (gameEntity.getGameEntityType().equals("unit")){
            player.setCurrentUnit(player.getCurrentUnit()+1);
        }
        if (gameEntity.getGameEntityType().equals("building")){
            map.getBuildings().add(gameEntity);
        }

        for (GameEntity gameEntity1 : player.getGameEntities()){
            if (gameEntity1.getGameEntityName().equals("headquarter")){
                calculationService.calculateCooldownTime(gameEntity1);
                gameEntityService.saveGameEntity(gameEntity1);
            }
        }


        gridService.saveGrid(locGrid);
        playerService.savePlayer(player);
        mapService.saveMap(map);
    }

    public void build(Player player, Map map, int x, int y, String name){
        Grid locGrid = map.getGridByGridIndex(x,y);


        GameEntity gameEntity = gameEntityService.createGameEntity(name,player,locGrid,x,y);

        locGrid.setGameEntity(gameEntity);

        //update player
        player.getGameEntities().add(gameEntity);
        if (gameEntity.getGameEntityType().equals("unit")){
            player.setCurrentUnit(player.getCurrentUnit()+1);
        }
        if (gameEntity.getGameEntityType().equals("building")){
            map.getBuildings().add(gameEntity);
        }
        for (GameEntity gameEntity1 : player.getGameEntities()){
            if (gameEntity1.getGameEntityName().equals("headquarter")){
                calculationService.calculateCooldownTime(gameEntity1);
                gameEntityService.saveGameEntity(gameEntity1);
            }
        }


        gridService.saveGrid(locGrid);
        playerService.savePlayer(player);
        mapService.saveMap(map);
    }

    /**
     * call merge function made by sky to merge the src into the dest
     * then we set srcGrid to null
     * we then delete the src gameEntity
     * lower the count of current unit of player
     * why lower count and not chnage the set? cus the set look and take from the db so we dont care
     * @param player
     * @param src
     * @param dest
     * @param map
     */
    public void merge(Player player, Map map, String[] src, String[] dest){
        Grid srcGrid  = getGridFromMap(map, src);
        Grid destGrid  = getGridFromMap(map, dest);

        GameEntity srcG = srcGrid.getGameEntity();
        GameEntity destG = destGrid.getGameEntity();

        //throw the 2 game entity into the function
        calculationService.setStatPostMerge(srcG,destG);

        srcGrid.setGameEntity(null);
        gameEntityService.deleteGameEntity(srcG);
        player.setCurrentUnit(player.getCurrentUnit()-1);
        player.getGameEntities().remove(srcG);
        gameEntityService.saveGameEntity(destG);
        gridService.saveGrid(srcGrid);
        gridService.saveGrid(destGrid);
        playerService.savePlayer(player);
        mapService.saveMap(map);
    }

    /**
     * we assume that the map sorted the list in the gridIndex order
     * @param loc
     * @param map
     * @return
     */
    public Grid getGridFromMap( Map map, String[] loc){
        int columns = map.getNumberOfColumns();
        int locX = Integer.parseInt(loc[0]);
        int locY = Integer.parseInt(loc[1]);
        int locIndex = locX + locY * columns;
        return  map.getGrids().get(locIndex);
    }

    public boolean checkIfAllPlayerCheckEnd(Room room){
        int size = room.getPlayers().size();
        int end = 0;
        for (Player player : room.getPlayers()){
            if (player.isCheckEnd()){
                end++;
            }
        }
        if (end == size){
            return true;
        }
        return false;
    }

    public boolean checkGameOver(Room room){
        int size = room.getPlayers().size();
        int lost = 0;
        for (Player player : room.getPlayers()){
            if (!player.isAlive()){
                lost++;
            }
        }
        if (lost == size-1){
            return true;
        }
        return false;
    }

}
