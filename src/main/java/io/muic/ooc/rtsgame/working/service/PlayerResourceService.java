package io.muic.ooc.rtsgame.working.service;

import io.muic.ooc.rtsgame.working.model.Player;
import io.muic.ooc.rtsgame.working.model.PlayerResource;
import io.muic.ooc.rtsgame.working.model.Resource;
import io.muic.ooc.rtsgame.working.repo.PlayerResourceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by wit on 3/22/2017 AD.
 */
@Service
public class PlayerResourceService {
    @Autowired
    PlayerResourceRepository playerResourceRepository;

    public PlayerResource findById(long id){
        return playerResourceRepository.findOne(id);
    }

    public PlayerResource findByPlayerIdAndResouceId(Player player, Resource resource){
        return playerResourceRepository.findByPlayerAndResource(player,resource);
    }

    public PlayerResource createPlayerResource(Player player, Resource resource, int initialValue){
        PlayerResource playerResource = new PlayerResource();
        playerResource.setPlayer(player);
        playerResource.setResource(resource);
        playerResource.setValue(initialValue);
        playerResourceRepository.save(playerResource);
        return playerResource;
    }

    public void deletePlayerResource(long id){
        playerResourceRepository.delete(id);
    }
}
