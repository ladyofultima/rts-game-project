package io.muic.ooc.rtsgame.working.service;

import io.muic.ooc.rtsgame.working.model.Grid;
import io.muic.ooc.rtsgame.working.model.GridResource;
import io.muic.ooc.rtsgame.working.model.Resource;
import io.muic.ooc.rtsgame.working.repo.GridResourceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by wit on 3/21/2017 AD.
 */
@Service

public class GridResourceService {
    @Autowired
    GridResourceRepository gridResourceRepository;

    public GridResource createGridResource(Grid grid, Resource resource, int initialValue){
        GridResource gridResource = new GridResource();
        gridResource.setResource(resource);
        gridResource.setGrid(grid);
        gridResource.setValue(initialValue);
        gridResourceRepository.save(gridResource);
        return gridResource;
    }

    public void deleteGridResource(long id){
        gridResourceRepository.delete(id);
    }
}
