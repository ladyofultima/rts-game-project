package io.muic.ooc.rtsgame.working.service;

import io.muic.ooc.rtsgame.working.model.Grid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Trung on 3/28/2017.
 */
@Service
public class GridIndexSortService {

    private List<Grid> a;
    private int n;
    private int left;
    private int right;
    private int largest;

    private void switchPlace(int i, int j){
        Grid toTrade = a.get(i);

        a.set(i, a.get(j));
        a.set(j, toTrade);
    }
    
    private void heapBuild(List<Grid> a){
        n = a.size() - 1;

        for(int i = n/2; i >= 0; i--){
            maxHeap(a, i);
        }
    }

    private void maxHeap(List<Grid> a, int i){
        left = 2*i;
        right = 2*i+1;

        if(left <= n && a.get(left).getGridIndex() > a.get(i).getGridIndex()){
            largest = left;
        } else {
            largest = i;
        }

        if(right <= n && a.get(right).getGridIndex() > a.get(largest).getGridIndex()){
            largest = right;
        }

        if(largest!=i){
            switchPlace(i,largest);
            maxHeap(a, largest);
        }
    }

    public void sortArray2(List<Grid> a0){
        a = a0;
        heapBuild(a);

        for(int i = n; i > 0; i--){
            switchPlace(0, i);
            n = n-1;
            maxHeap(a, 0);
        }
    }

    public void sortArray(List<Grid> grids){
        grids.sort((s1,s2) -> s1.getGridIndex() - s2.getGridIndex());
    }
}
