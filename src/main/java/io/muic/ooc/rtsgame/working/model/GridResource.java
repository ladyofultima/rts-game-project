package io.muic.ooc.rtsgame.working.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

/**
 * Created by wit on 3/20/2017 AD.
 */

@Entity
public class GridResource {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "grid_resource_id")
    long gridResourceId;

    @OneToOne
//    @PrimaryKeyJoinColumn
    @JsonIgnore
    private Grid grid;

    @OneToOne
//    @PrimaryKeyJoinColumn
    @JsonIgnore
    private Resource resource;

    @Column(name  = "value")
    int value;


    public Grid getGrid() {
        return grid;
    }

    public void setGrid(Grid grid) {
        this.grid = grid;
    }

    public Resource getResource() {
        return resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
