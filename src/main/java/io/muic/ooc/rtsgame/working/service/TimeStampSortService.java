package io.muic.ooc.rtsgame.working.service;

import io.muic.ooc.rtsgame.working.model.GameEntity;
import io.muic.ooc.rtsgame.working.model.Grid;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Trung on 3/28/2017.
 */

@Service
public class TimeStampSortService {

    private List<GameEntity> a;
    private int n;
    private int left;
    private int right;
    private int largest;

    private void switchPlace(int i, int j){
        GameEntity toTrade = a.get(i);

        a.set(i, a.get(j));
        a.set(j, toTrade);
    }

    private void heapBuild(List<GameEntity> a){
        n = a.size() - 1;

        for(int i = n/2; i >= 0; i--){
            maxHeap(a, i);
        }
    }

    private void maxHeap(List<GameEntity> a, int i){
        left = 2*i;
        right = 2*i+1;

        if(left <= n && a.get(left).getBuildTimeStamp() > a.get(i).getBuildTimeStamp()){
            largest = left;
        } else {
            largest = i;
        }

        if(right <= n && a.get(right).getBuildTimeStamp() > a.get(largest).getBuildTimeStamp()){
            largest = right;
        }

        if(largest!=i){
            switchPlace(i,largest);
            maxHeap(a, largest);
        }
    }

    public void sortArray(List<GameEntity> a0){
        a = a0;
        heapBuild(a);

        for(int i = n; i > 0; i--){
            switchPlace(0, i);
            n = n-1;
            maxHeap(a, 0);
        }
    }

//    public void sortArray(List<GameEntity> gameEntities){
//        gameEntities.sort((s1,s2) -> s2.getBuildTimeStamp() - s1.getBuildTimeStamp());
//    }
}
