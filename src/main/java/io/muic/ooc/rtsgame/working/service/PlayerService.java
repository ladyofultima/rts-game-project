package io.muic.ooc.rtsgame.working.service;

import io.muic.ooc.rtsgame.online.model.Room;
import io.muic.ooc.rtsgame.online.model.User;
import io.muic.ooc.rtsgame.working.factory.PlayerFactory;
import io.muic.ooc.rtsgame.working.model.Player;
import io.muic.ooc.rtsgame.working.repo.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by wit on 3/22/2017 AD.
 */
@Service
public class PlayerService {

    @Autowired
    PlayerRepository playerRepository;

    /**
     * user id and player id are one and the same
     * @param id
     * @return
     */
    public Player findById(long id){
        return  playerRepository.findOne(id);
    }

    public Player findByUserId(long id){
        return findById(id);
    }

    /**
     * TODO: Set up other stat of the player
     * given a user and a room, create new player and set the id of the player to be the same as user
     * automatically save the player to the database
     * it will set everything up for the player and save it to the database
     * it will also return the player (i figured they might wanna do something with it
     * @param user
     * @return
     */
    public Player createPlayer(User user, Room room){
        Player player = PlayerFactory.createPlayer(room,user);
        playerRepository.save(player);
        return player;
    }

    public void  savePlayer(Player player){
        playerRepository.save(player);
    }

    public void playerCheckEnd(Player player){
        player.setCheckEnd(true);
        playerRepository.save(player);
    }


    public void deletePlayer(long id){
        playerRepository.delete(id);
    }
    public void deletePlayer(User user){
        deletePlayer(user.getId());
    }
}
