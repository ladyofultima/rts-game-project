package io.muic.ooc.rtsgame.working.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.muic.ooc.rtsgame.online.model.Room;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.List;

/**
 * Created by wit on 3/14/2017 AD.
 */
@Entity
public class Map {
    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "map_id")
    long mapId;

    @Column(name = "number_of_rows")
    int numberOfRows;

    @Column(name  = "number_of_columns")
    int numberOfColumns;

    @Column(name = "total_number_of_grids")
    int totalNumberOfGrids;

    @Column(name = "state_id")
    String stateId;

    @OneToMany
    @JsonIgnore
    List<Grid> grids;

    @OneToMany
    @JsonIgnore
    List<GameEntity> buildings;

    @OneToOne
    @PrimaryKeyJoinColumn
    @JsonIgnore
    Room room;

    @Column(name = "ready")
    boolean isReady = false;

    public boolean isReady() {
        return isReady;
    }

    public void setReady(boolean ready) {
        isReady = ready;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public int getNumberOfRows() { return numberOfRows; }

    public void setNumberOfRows(int numberOfRows) { this.numberOfRows = numberOfRows; }

    public int getNumberOfColumns() { return numberOfColumns; }

    public void setNumberOfColumns(int numberOfColumns) { this.numberOfColumns = numberOfColumns; }

    public int getTotalNumberOfGrids() { return totalNumberOfGrids; }

    public void setTotalNumberOfGrids(int totalNumberOfGrids) { this.totalNumberOfGrids = totalNumberOfGrids; }

    public List<Grid> getGrids() { return grids; }

    public void setGrids(List<Grid> grids) { this.grids = grids; }

    public long getMapId() { return mapId;  }

    public void setMapId(long mapId) { this.mapId = mapId; }

    public List<GameEntity> getBuildings() { return buildings; }

    public void setBuildings(List<GameEntity> buildings) { this.buildings = buildings; }

    public Grid getGridByGridIndex(int x, int y){
        return grids.get(x + y * getNumberOfColumns());
    }

}
