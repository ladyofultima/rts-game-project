package io.muic.ooc.rtsgame.working.repo;

import io.muic.ooc.rtsgame.working.model.Player;
import io.muic.ooc.rtsgame.working.model.Resource;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by wit on 3/16/2017 AD.
 */
public interface ResourceRepository extends CrudRepository<Resource, Long>{
    Resource findByResourceName(String resourceName);
}
