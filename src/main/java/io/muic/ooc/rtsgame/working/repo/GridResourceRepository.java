package io.muic.ooc.rtsgame.working.repo;

import io.muic.ooc.rtsgame.working.model.Grid;
import io.muic.ooc.rtsgame.working.model.GridResource;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by wit on 3/20/2017 AD.
 */
@Repository
public interface GridResourceRepository extends CrudRepository<GridResource, Long> {
    GridResource findByGrid(Grid grid);
}
