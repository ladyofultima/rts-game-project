package io.muic.ooc.rtsgame.working.repo;

import io.muic.ooc.rtsgame.online.model.User;
import io.muic.ooc.rtsgame.working.model.Player;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by wit on 3/16/2017 AD.
 */
@Repository
public interface PlayerRepository  extends CrudRepository<Player, Long> {
    Player findPlayerByUser(User user);
}
