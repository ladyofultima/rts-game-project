package io.muic.ooc.rtsgame.working.service;

import io.muic.ooc.rtsgame.online.model.Room;
import io.muic.ooc.rtsgame.online.model.User;
import io.muic.ooc.rtsgame.online.service.RoomService;
import io.muic.ooc.rtsgame.online.service.UserService;
import io.muic.ooc.rtsgame.working.factory.GameEntityFactory;
import io.muic.ooc.rtsgame.working.model.GameEntity;
import io.muic.ooc.rtsgame.working.model.Grid;
import io.muic.ooc.rtsgame.working.model.Map;
import io.muic.ooc.rtsgame.working.model.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;

/**
 * Created by wit on 3/23/2017 AD.
 */
@Service
public class ValidActionService {

    @Autowired
    GameService gameService;
    @Autowired
    UserService userService;
    @Autowired
    RoomService roomService;
    @Autowired
    MapService mapService;
    @Autowired
    PlayerService playerService;


    /**
     * isValidRequest takes in Principal principal and use it to check the validity of the request
     * @return
     */
    public boolean isValidRequest(Principal principal){
        User user = userService.findByUsername(principal.getName());
        if (user == null){
            return  false;
        }

        Room room = user.getRoom();
        if (room == null){
            return  false;
        }
        return true;
    }

    public boolean isValidCreate(Principal principal){
        if (!isValidRequest(principal)){
            return false;
        }

        User user = userService.findByUsername(principal.getName());
        Room room = user.getRoom();
        Map map = mapService.findById(room.getId());
        if (map != null){
            return  false;
        }

        return  true;
    }

    public boolean isValidAction(Principal principal){
        if (!isValidRequest(principal)){
            return  false;
        }

        User user = userService.findByUsername(principal.getName());
        Room room = user.getRoom();
        Map map = mapService.findById(room.getId());
        Player player = playerService.findById(user.getId());
        if (player == null || map == null || !map.isReady() || !player.isAlive()){
            return  false;
        }
        return  true;
    }

    public boolean isValidShow(Principal principal){
        if (!isValidRequest(principal)){
            return  false;
        }

        User user = userService.findByUsername(principal.getName());
        Room room = user.getRoom();
        Map map = mapService.findById(room.getId());
        Player player = playerService.findById(user.getId());
        if (player == null || map == null || !map.isReady()){
            return  false;
        }
        return  true;
    }
    /**
     * get the location, and check its validity within the map
     * @param map
     * @param loc
     * @return
     */
    public boolean isValidLocation(Map map, String[] loc){
        int locX = Integer.parseInt(loc[0]);
        int locY = Integer.parseInt(loc[1]);
        if (locX >= map.getNumberOfColumns() || locY >= map.getNumberOfRows() || locX < 0 || locY < 0){
            return false;
        }
        return true;
    }

    /**
     * check validity of the name given
     * check if the location is null (has no entity)
     * check if the territory is player's territory
     * check if the player have enough gold
     * @param player
     * @param map
     * @param loc
     * @param name
     * @return
     */
    public boolean isValidBuild(Player player, Map map, String[] loc, String name){
        if (!isValidLocation(map,loc)){
            return  false;
        }

        Grid grid = gameService.getGridFromMap(map,loc);
        if (grid.getGameEntity() != null || grid.getOwner() == null || grid.getOwner().getPlayerId() != player.getPlayerId()){
            return false;
        }

        if (!GameEntityFactory.hasName(name)){
            return false;
        }

        if (GameEntityFactory.isUnit(name) && player.getCurrentUnit() >= player.getMaximumUnit()){
            return false;
        }

        for (GameEntity gameEntity : player.getGameEntities()){
            if (gameEntity.getGameEntityName().equals("headquarter") && gameEntity.getLastActionTimeStamp() > (double) System.nanoTime()){
                return false;
            }else if(gameEntity.getGameEntityName().equals("headquarter")){
                break;
            }
        }

        return true;
    }

    /**
     * check valid
     * check srcGrid has entity, that entity is player's controlled, and can merge
     * check destGrid
     * check srcGrid Entity = destGrid Entity
     * check cooldown
     * @param player
     * @param map
     * @param src
     * @param dest
     * @return
     */
    public boolean isValidMerge(Player player, Map map, String[] src, String[] dest){

        if (!isValidLocation(map,src) && !isValidLocation(map,dest)){
            return  false;
        }

        Grid srcGrid = gameService.getGridFromMap(map,src);
        Grid destGrid = gameService.getGridFromMap(map,dest);

        //check srcGrid entity
        if (srcGrid.getGameEntity() == null || srcGrid.getGameEntity().getGameEntityType().equals("obstacle") ||
                srcGrid.getGameEntity().getOwner().getPlayerId() != player.getPlayerId() ||
                !srcGrid.getGameEntity().isCanMerge()){
            return false;
        }

        if (destGrid.getGameEntity() == null || destGrid.getGameEntity().getGameEntityType().equals("obstacle") ||
                destGrid.getGameEntity().getOwner().getPlayerId() != player.getPlayerId() ||
                !destGrid.getGameEntity().isCanMerge()){
            return false;
        }

        if (Math.abs(srcGrid.getPosX() - destGrid.getPosX()) + Math.abs(srcGrid.getPosY() - destGrid.getPosY()) > srcGrid.getGameEntity().getMoveRange()){
            return  false;
        }

        if (!destGrid.getGameEntity().getGameEntityName().equals(srcGrid.getGameEntity().getGameEntityName())){
            return false;
        }

        if (srcGrid.getGameEntity().getLastActionTimeStamp() > (double) System.nanoTime()){
            return false;
        }

        return true;
    }

    /**
     * check valid location
     * check if src has the entity and that entity can move and it is player's entity
     * check if dest has no entity
     * @param player
     * @param map
     * @param src
     * @param dest
     * @return
     */
    public boolean isValidMove(Player player, Map map, String[] src, String[] dest){


        if (!isValidLocation(map,src) && !isValidLocation(map,dest)){
            return  false;
        }

        Grid srcGrid = gameService.getGridFromMap(map,src);
        Grid destGrid = gameService.getGridFromMap(map,dest);

        if (srcGrid.getGameEntity() == null || !srcGrid.getGameEntity().isCanMove() || srcGrid.getGameEntity().getGameEntityType().equals("obstacle")
                || srcGrid.getGameEntity().getOwner().getPlayerId() != player.getPlayerId()){
            return  false;
        }

        if (destGrid.getGameEntity() != null){
            return false;
        }

        if (Math.abs(srcGrid.getPosX() - destGrid.getPosX()) + Math.abs(srcGrid.getPosY() - destGrid.getPosY()) > srcGrid.getGameEntity().getMoveRange()){
            return  false;
        }

        if (srcGrid.getGameEntity().getLastActionTimeStamp() > (double)  System.nanoTime()){
            return false;
        }

        return true;
    }

    /**
     * check valid location
     * check if src has the entity and that entity can attack and that entity is of players' controlled
     * check if dest has entity, is not an obstacle, is not player's controlled
     * @param player
     * @param map
     * @param src
     * @param dest
     * @return
     */
    public boolean isValidAttack(Player player, Map map, String[] src, String[] dest){
        //check validity of location
        if(!isValidLocation(map,src) && !isValidLocation(map,dest)){
            return  false;
        }

        Grid srcGrid = gameService.getGridFromMap(map,src);
        Grid destGrid = gameService.getGridFromMap(map,dest);

        //src checking
        if (srcGrid.getGameEntity() == null || !srcGrid.getGameEntity().isCanAttack() ||
                srcGrid.getGameEntity().getGameEntityType().equals("obstacle") ||
                srcGrid.getGameEntity().getOwner().getPlayerId() != player.getPlayerId()){
            return false;
        }

        //dest checking
        if (destGrid.getGameEntity() == null || destGrid.getGameEntity().getGameEntityType().equals("obstacle")
                || destGrid.getGameEntity().getOwner().getPlayerId() == player.getPlayerId()){
            return  false;
        }

        if (Math.abs(srcGrid.getPosX() - destGrid.getPosX()) + Math.abs(srcGrid.getPosY() - destGrid.getPosY()) > srcGrid.getGameEntity().getAttackRange()){
            return  false;
        }

        if (srcGrid.getGameEntity().getLastActionTimeStamp() > (double) System.nanoTime()){
            return false;
        }

        return  true;
    }
}
