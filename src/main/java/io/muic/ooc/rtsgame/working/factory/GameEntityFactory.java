package io.muic.ooc.rtsgame.working.factory;

import io.muic.ooc.rtsgame.working.model.GameEntity;
import io.muic.ooc.rtsgame.working.model.Player;

import java.util.*;

/**
 * Created by wit on 3/21/2017 AD.
 */
public class GameEntityFactory {

    public static String[] names = {"barrack","farm","headquarter","outpost","tower","scout","soldier","tank","miner"};

    public static String[] buildings = {"barrack","farm","headquarter","outpost","tower"};

    public static String[] units = {"scout","soldier","tank","miner"};

    public static boolean isBuilding(String name){
        for (String n: buildings){
            if (name.equals(n)){
                return true;
            }
        }
        return  false;
    }
    public static boolean isUnit(String name){
        for (String n: units){
            if (name.equals(n)){
                return true;
            }
        }
        return  false;
    }

    public static boolean hasName(String name){
        for (String n : names){
            if (n.equals(name)){
                return true;
            }
        }
        return false;
    }

    /**
     * Create all manner of things inside the map excluding natural resource
     * @param entityName requested name to identify what entity is requested
     * @param player player requesting action
     * @param posX requested spawn position
     * @param posY requested spawn position
     * @return
     */
    public static GameEntity createEntity(String entityName, Player player, int posX, int posY) {

        final String BUILDING_ENTITY_TYPE = "building";
        final String UNIT_ENTITY_TYPE = "unit";
        final String OBSTACLE_ENTITY_TYPE = "obstacle";
        final double BUILD_TIME_STAMP = System.nanoTime();

        Set<GameEntity> playerEntity = player.getGameEntities();

        switch (entityName) {

            case "barrack":
                GameEntity barrack = new GameEntity();

                //Entity Type Setting
                barrack.setAllType(false, false, false, true, false, false);

                //Building Setting
                barrack.setAllBasic(posX, posY, 50, 3, 50,
                        player, 2, "barrack", BUILDING_ENTITY_TYPE);
                barrack.setTerritoryRange(3);
                barrack.setBuildTimeStamp(BUILD_TIME_STAMP);

                //Add to player's attribute
                player.setMaximumUnit(player.getMaximumUnit() + 5);
                return barrack;

            case "farm":
                GameEntity farm = new GameEntity();

                //Entity Type Setting
                farm.setAllType(false, false, false, true, false, false);

                //Building Setting
                farm.setAllBasic(posX, posY, 20, 1, 20,
                        player, 0, "farm", BUILDING_ENTITY_TYPE);
                farm.setTerritoryRange(1);
                farm.setBuildTimeStamp(BUILD_TIME_STAMP);

                //Add to player's attribute
                player.setGoldPerSecond(player.getGoldPerSecond() + 5);
                return farm;

            case "headquarter":
                GameEntity headquarter = new GameEntity();


                //Entity Type Setting
                headquarter.setAllType(false, false, false, true, false, false);

                //Building Setting
                headquarter.setAllBasic(posX, posY, 70, 3, 70,
                        player, 5, "headquarter", BUILDING_ENTITY_TYPE);
                headquarter.setTerritoryRange(3);
                headquarter.setAllActionable(0,6 * 1e9);
                headquarter.setBuildTimeStamp(BUILD_TIME_STAMP);
                return headquarter;

            case "outpost":
                GameEntity outpost = new GameEntity();

                //Entity Type Setting
                outpost.setAllType(false, false, false, true, false, false);

                //Building Setting
                outpost.setAllBasic(posX, posY, 50, 4, 50,
                        player, 2, "outpost", BUILDING_ENTITY_TYPE);
                outpost.setTerritoryRange(4);
                outpost.setBuildTimeStamp(BUILD_TIME_STAMP);
                return outpost;

            case "tower":
                GameEntity tower = new GameEntity();


                //Entity Type Setting
                tower.setAllType(true, false, false, true, false, false);

                //Building Setting
                tower.setAllBasic(posX, posY, 100, 3, 100,
                        player, 2, "tower", BUILDING_ENTITY_TYPE);
                tower.setAllAttackable(5, 3);
                tower.setAllActionable(0,3 * 1e9);
                tower.setBuildTimeStamp(BUILD_TIME_STAMP);
                return tower;

            case "scout":
                GameEntity scout = new GameEntity();

                //Entity Type Setting
                scout.setAllType(true, true, false, false, true, false);

                //Unit Setting
                scout.setAllBasic(posX, posY, 10, 4, 10,
                        player, 0, "scout", UNIT_ENTITY_TYPE);
                scout.setAllAttackable(3, 4);
                scout.setMoveRange(4);
                scout.setAllActionable(0,5 * 1e9);

                player.setCurrentUnit(player.getCurrentUnit() + 1);

                //Add to player's existing Entity list
                playerEntity.add(scout);
                player.setGameEntities(playerEntity);

                return scout;

            case "soldier":
                GameEntity soldier = new GameEntity();

                //Entity Type Setting
                soldier.setAllType(true, true, false, false, true, false);

                //Unit Setting
                soldier.setAllBasic(posX, posY, 20, 2, 20,
                        player, 2, "soldier", UNIT_ENTITY_TYPE);
                soldier.setAllAttackable(5, 3);
                soldier.setMoveRange(2);
                soldier.setAllActionable(0,5 * 1e9);

                player.setCurrentUnit(player.getCurrentUnit() + 1);

                //Add to player's existing Entity list
                playerEntity.add(soldier);
                player.setGameEntities(playerEntity);

                return soldier;

            case "tank":
                GameEntity tank = new GameEntity();

                //Entity Type Setting
                tank.setAllType(true, true, false, false, true, false);

                //Unit Setting
                tank.setAllBasic(posX, posY, 40, 2, 40,
                        player, 5, "tank", UNIT_ENTITY_TYPE);
                tank.setAllAttackable(8, 2);
                tank.setMoveRange(2);
                tank.setAllActionable(0,5 * 1e9);

                player.setCurrentUnit(player.getCurrentUnit() + 1);

                //Add to player's existing Entity list
                playerEntity.add(tank);
                player.setGameEntities(playerEntity);

                //Add to player's existing Entity list
                playerEntity.add(tank);
                player.setGameEntities(playerEntity);

                return tank;

            case "miner":
                GameEntity miner = new GameEntity();

                //Entity Type Setting
                miner.setAllType(false, true, true, false, true, false);

                //Unit Setting
                miner.setAllBasic(posX, posY, 10, 2, 10,
                        player, 0, "miner", UNIT_ENTITY_TYPE);
                miner.setGatherRate(10);
                miner.setMoveRange(2);
                miner.setAllActionable(0,5 * 1e9);

                player.setCurrentUnit(player.getCurrentUnit() + 1);

                //Add to player's existing Entity list
                playerEntity.add(miner);
                player.setGameEntities(playerEntity);

                return miner;

            case "obstacle":
                GameEntity obstacle = new GameEntity();

                //Entity Type Setting
                obstacle.setAllType(false, false, false, false, false, true);

                //Obstacle Setting
                obstacle.setAllObstacle(posX, posY, true, "obstacle", OBSTACLE_ENTITY_TYPE);

                return obstacle;

            default:
                return null;
        }
    }

}
