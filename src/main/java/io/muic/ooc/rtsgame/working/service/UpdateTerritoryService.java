package io.muic.ooc.rtsgame.working.service;

import io.muic.ooc.rtsgame.working.model.GameEntity;
import io.muic.ooc.rtsgame.working.model.Grid;
import io.muic.ooc.rtsgame.working.model.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Trung on 4/2/2017.
 */
@Service
public class UpdateTerritoryService {
    @Autowired
    TimeStampSortService timeStampSortService;

    @Autowired
    GridService gridService;

    @Autowired
    MapService mapService;


    /**
     * Do setTerritoryOwner on all buildings within given map
     * @param map
     */
    public void updateTerritory(Map map) {

        List<GameEntity> allBuildings = map.getBuildings();
        timeStampSortService.sortArray(allBuildings);

        resetGridOwner(map);

        for (GameEntity building: allBuildings) {
            setTerritoryOwner(building, map);
        }

        for (Grid grid: map.getGrids()){
            gridService.saveGrid(grid);
        }
        mapService.saveMap(map);
    }

    /**
     * Set grid's owner to building's owner if grid's within building's range and is not yet occupied
     * @param building requested building
     * @param map
     */
    private void setTerritoryOwner(GameEntity building, Map map){

        final int TERRITORY_RANGE = building.getTerritoryRange();
        List<Grid> gridList = map.getGrids();


        for (Grid grid: gridList) {

            int gridX = grid.getPosX();
            int gridY = grid.getPosY();
            int distance = Math.abs(gridX - building.getPosX()) + Math.abs(gridY - building.getPosY());

            if (distance <= TERRITORY_RANGE && grid.getOwner() == null) {
                grid.setOwner(building.getOwner());
            }
        }
    }

    /**
     * make all grid.getOwner() == null
     * @param map
     */
    private void resetGridOwner(Map map) {
        for (Grid grid: map.getGrids()) {
            grid.setOwner(null);
        }
    }
}
