package io.muic.ooc.rtsgame.working.factory;

import io.muic.ooc.rtsgame.online.model.Room;
import io.muic.ooc.rtsgame.online.model.User;
import io.muic.ooc.rtsgame.working.model.GameEntity;
import io.muic.ooc.rtsgame.working.model.Player;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by wit on 3/21/2017 AD.
 */
public class PlayerFactory {

    public static Player createPlayer(Room room, User user){

        Player player = new Player();
        Set<GameEntity> blankGameEntityList = new HashSet<>();
        player.setPlayerId(user.getId());
        player.setCurrentUnit(0);
        player.setGoldPerSecond(10);
        player.setMaximumUnit(5);
        player.setGameEntities(blankGameEntityList);
        player.setPlayerId(user.getId());
        player.setRoom(room);
        player.setUser(user);
        player.setAlive(true);

        return player;
    }
}
