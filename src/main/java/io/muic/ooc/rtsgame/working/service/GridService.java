package io.muic.ooc.rtsgame.working.service;

import io.muic.ooc.rtsgame.working.model.Grid;
import io.muic.ooc.rtsgame.working.model.Map;
import io.muic.ooc.rtsgame.working.repo.GridRepository;
import io.muic.ooc.rtsgame.working.repo.MapRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by wit on 3/21/2017 AD.
 */
@Service
public class GridService {
    @Autowired
    GridRepository gridRepository;

    public void saveGrid(Grid grid){
        gridRepository.save(grid);
    }

    public Grid createGrid(Map map, int x, int y){
        //c is x
        //r is y
        int c = map.getNumberOfColumns();
        int r = map.getNumberOfRows();
        if (x > c || y > r){
            return  null;
        }
        int inGameId = c * y + x;
        Grid grid = new Grid();
        grid.setPosX(x);
        grid.setPosY(y);
        grid.setGridIndex(inGameId);
        saveGrid(grid);
        return grid;
    }

    public void deleteGrid(Grid grid){
        gridRepository.delete(grid.getGridId());
    }
}
