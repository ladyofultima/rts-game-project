package io.muic.ooc.rtsgame.working.repo;

import io.muic.ooc.rtsgame.working.model.Player;
import io.muic.ooc.rtsgame.working.model.PlayerResource;
import io.muic.ooc.rtsgame.working.model.Resource;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by wit on 3/16/2017 AD.
 */
@Repository
public interface PlayerResourceRepository extends CrudRepository<PlayerResource, Long> {
    PlayerResource findByPlayerAndResource(Player player, Resource  resource);
}
