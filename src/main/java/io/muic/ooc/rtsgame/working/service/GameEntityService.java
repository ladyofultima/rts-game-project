package io.muic.ooc.rtsgame.working.service;

import io.muic.ooc.rtsgame.working.factory.GameEntityFactory;
import io.muic.ooc.rtsgame.working.model.GameEntity;
import io.muic.ooc.rtsgame.working.model.Grid;
import io.muic.ooc.rtsgame.working.model.Map;
import io.muic.ooc.rtsgame.working.model.Player;
import io.muic.ooc.rtsgame.working.repo.GameEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by wit on 3/21/2017 AD.
 */
@Service
public class GameEntityService {
    @Autowired
    GameEntityRepository gameEntityRepository;

    /**
     * Doing these when GamEntity factory is done
     * @param entityName
     * @param x
     * @param y
     * @return
     */
    public GameEntity createGameEntity(String entityName, Player player, Grid grid,int x, int y){
        GameEntity gameEntity = GameEntityFactory.createEntity(entityName, player,x,y);
        grid.setGameEntity(gameEntity);
        gameEntity.setGrid(grid);
//        if (player != null ){
//            player.getGameEntities().add(gameEntity);
//        }
        gameEntityRepository.save(gameEntity);
        return gameEntity;
    }

    public void saveGameEntity(GameEntity gameEntity){
        if (gameEntity.getCurrentHealth() > 0){
            gameEntityRepository.save(gameEntity);
        }
    }

    public void deleteGameEntity(GameEntity gameEntity){
        gameEntityRepository.delete(gameEntity);
    }

    public GameEntity findById(long id){
        return  gameEntityRepository.findOne(id);
    }
}
