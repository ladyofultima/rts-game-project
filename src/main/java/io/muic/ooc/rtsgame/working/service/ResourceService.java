package io.muic.ooc.rtsgame.working.service;

import io.muic.ooc.rtsgame.working.model.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by wit on 3/22/2017 AD.
 */
@Service
public class ResourceService {

    @Autowired
    ResourceService resourceService;

    public Resource findById(long id){
        return  resourceService.findById(id);
    }

    public Resource findByName(String name){
        return  resourceService.findByName(name);
    }
}
