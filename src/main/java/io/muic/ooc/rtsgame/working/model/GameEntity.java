package io.muic.ooc.rtsgame.working.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;


/**
 * Created by wit on 3/16/2017 AD.
 */

/**
 * GameEntity class is the blueprint for all the unit/building in the game
 * We divide the stats into different areas: general, attack related, move related, territory related,
 * resource gathering related
 */
@Entity
public class GameEntity {
    /**
     * gameEntityId is auto-generated
     * gameEntityName is the generic name of the entity (ie: farm)
     * gameEntityType define the type of the entity (building vs unit)
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "game_entity_id")
    long gameEntityId;

    //Generic name (farm, hq, solider etc)
    @Column(name  = "game_entity_name")
    String gameEntityName;

    //Identify if it was building or unit
    @Column(name  = "game_entity_type")
    String gameEntityType;

    @OneToOne(mappedBy="gameEntity")
    @PrimaryKeyJoinColumn
    @JsonIgnore
    private Grid grid;

    //general stats
    @Column(name = "position_x")
    int posX;
    @Column(name = "postion_y")
    int posY;

    @Column(name  = "current_health")
    int currentHealth;
    @Column(name = "max_health")
    int maxHealth;

    @Column(name  = "vision_range")
    int visionRange;

    @ManyToOne
    @JoinColumn(name  = "player_id")
    @NotFound(action=NotFoundAction.IGNORE)
    @JsonIgnore
    Player owner;

    @Column(name  = "defense")
    int defense;

    @Column(name = "action_cooldown_time")
    double actionCooldownTime;
    @Column(name = "last_action_timestamp")
    double lastActionTimeStamp;


    //merge related
    boolean canMerge;
    int mergeAmount;

    //attack related
    @Column(name = "can_attack")
    boolean canAttack;
    @Column(name = "attack")
    int attack;
    @Column(name = "attack_range")
    int attackRange;

    //move related
    @Column(name = "can_move")
    boolean canMove;
    @Column(name = "move_range")
    int moveRange;

    //territory related
    @Column(name = "can_give_territory")
    boolean canGiveTerritory;
    @Column(name = "territory_range")
    int territoryRange;
    @Column(name = "build_time_stamp")
    double buildTimeStamp;

    //resource gathering related
    @Column(name = "can_gather_resource")
    boolean canGatherResource;
    @Column(name = "gather_rate")
    int gatherRate;

    //obstacle
    @Column(name = "is_obstacle")
    boolean isObstacle;

    @Column(name = "coolDownTimeLeft")
    private double coolDownTimeLeft;

    public double getCoolDownTimeLeft() {
        setCoolDownTimeLeft();
        return coolDownTimeLeft;
    }
    
    public void setCoolDownTimeLeft() {
        double time = System.nanoTime();
        if (getLastActionTimeStamp() - time > 0){
            coolDownTimeLeft = getLastActionTimeStamp() - time;
        }else{
            coolDownTimeLeft = 0;
        }
    }

    //Basic Unit Setting
    public void setPosX(int posX) { this.posX = posX; }

    public void setPosY(int posY) { this.posY = posY; }

    public void setCurrentHealth(int currentHealth) { this.currentHealth = currentHealth; }

    public void setVisionRange(int visionRange) { this.visionRange = visionRange; }

    public Grid getGrid() {
        return grid;
    }

    public void setGrid(Grid grid) {
        this.grid = grid;
    }

    public void setMaxHealth(int maxHealth) { this.maxHealth = maxHealth; }

    public void setOwner(Player owner) { this.owner = owner; }

    public void setDefense(int defense) { this.defense = defense; }

    public void setGameEntityName(String gameEntityName) { this.gameEntityName = gameEntityName; }

    public void setGameEntityType(String gameEntityType) { this.gameEntityType = gameEntityType; }

    public void setGameEntityId(long gameEntityId) { this.gameEntityId = gameEntityId; }

    public void setAllBasic(int posX, int posY, int currentHealth, int visionRange,
                            int maxHealth, Player owner, int defense, String gameEntityName,
                            String gameEntityType){

        setPosX(posX);
        setPosY(posY);
        setCurrentHealth(currentHealth);
        setVisionRange(visionRange);
        setMaxHealth(maxHealth);
        setOwner(owner);
        setDefense(defense);
        setGameEntityName(gameEntityName);
        setGameEntityType(gameEntityType);
    }

    public void setAllType(boolean canAttack, boolean canMerge,
                           boolean canGatherResource, boolean canGiveTerritory,
                           boolean canMove, boolean isObstacle){

        setCanAttack(canAttack);
        setCanMerge(canMerge);
        setCanGatherResource(canGatherResource);
        setCanGiveTerritory(canGiveTerritory);
        setCanMove(canMove);
        setIsObstacle(isObstacle);
    }

    public int getPosX() { return this.posX; }

    public int getPosY() { return this.posY; }

    public int getCurrentHealth() { return this.currentHealth; }

    public int getMaxHealth() { return this.maxHealth; }

    public int getVisionRange() { return this.visionRange; }

    public Player getOwner() { return this.owner; }

    public int getDefense() { return this.defense; }

    public String getGameEntityName() { return this.gameEntityName; }

    public String getGameEntityType() { return this.gameEntityType; }

    public long getGameEntityId() { return this.gameEntityId; }

    //Attackable Unit Setting
    public void setCanAttack(boolean canAttack) { this.canAttack = canAttack; }

    public void setAttack(int attack) { this.attack = attack; }

    public void setAttackRange(int attackRange) { this.attackRange = attackRange; }

    public void setAllAttackable(int attack, int attackRange){

        setAttack(attack);
        setAttackRange(attackRange);
    }

    public boolean isCanAttack() { return this.canAttack; }

    public int getAttack() { return this.attack; }

    public int getAttackRange() { return this.attackRange; }

    //Movable Unit Setting
    public void setCanMove(boolean canMove) { this.canMove = canMove; }

    public void setMoveRange(int moveRange) { this.moveRange = moveRange; }

    public boolean isCanMove() { return this.canMove; }

    public int getMoveRange() { return this.moveRange; }

    //Building Unit Setting
    public void setCanGiveTerritory(boolean canGiveTerritory) { this.canGiveTerritory = canGiveTerritory; }

    public void setTerritoryRange(int territoryRange) { this.territoryRange = territoryRange; }

    public void setBuildTimeStamp(double buildTimeStamp) { this.buildTimeStamp = buildTimeStamp; }

    public boolean isCanGiveTerritory() { return this.canGiveTerritory; }

    public int getTerritoryRange() { return this.territoryRange; }

    public double getBuildTimeStamp() { return this.buildTimeStamp; }

    //Gatherable Unit Setting
    public void setCanGatherResource(boolean canGatherResource) { this.canGatherResource = canGatherResource; }

    public void setGatherRate(int gatherRate) { this.gatherRate = gatherRate; }

    public boolean isCanGatherResource() { return this.canGatherResource; }

    public int getGatherRate() { return this.gatherRate; }

    //Actionable unit Setting
    public void setLastActionTimeStamp(double lastActionTimeStamp) { this.lastActionTimeStamp = lastActionTimeStamp; }

    public void setActionCooldownTime(double actionCooldownTime) { this.actionCooldownTime = actionCooldownTime; }

    public void setAllActionable(double lastActionTimeStamp, double actionCooldownTime){

        setLastActionTimeStamp(lastActionTimeStamp);
        setActionCooldownTime(actionCooldownTime);
    }

    //Obstacle Entity Setting
    public void setIsObstacle(boolean isObstacle) { this.isObstacle = isObstacle; }

    public boolean isObstacle() { return this.isObstacle; }

    public void setAllObstacle(int posX, int posY, boolean isObstacle, String gameEntityName, String gameEntityType) {

        setPosX(posX);
        setPosY(posY);
        setIsObstacle(isObstacle);
        setGameEntityName(gameEntityName);
        setGameEntityType(gameEntityType);
    }

    public double getLastActionTimeStamp() { return this.lastActionTimeStamp; }

    public double getActionCooldownTime() { return this.actionCooldownTime; }

    //Mergable Unit Setting
    public void setCanMerge(boolean canMerge) { this.canMerge = canMerge; }

    public void setMergeAmount(int mergeAmount) {
        this.mergeAmount = mergeAmount;
    }

    public boolean isCanMerge() { return this.canMerge; }

    public int getMergeAmount() {
        return this.mergeAmount;
    }
}
