package io.muic.ooc.rtsgame.working.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;

/**
 * Created by wit on 3/14/2017 AD.
 */

/**
 * grid_id is the unique id of the grid in the whole table
 * grid_index is the unique index of the grid in the map
 */
@Entity
public class Grid {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name =  "grid_id")
    private long gridId;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    private Player owner;

    @OneToOne
    @Cascade({ CascadeType.ALL })
    @JoinColumn(name = "game_entity_id")
    @NotFound(action=NotFoundAction.IGNORE)
    @JsonIgnore
    private GameEntity gameEntity;

    @OneToOne
    @Cascade({ CascadeType.ALL })
    @JoinColumn(name = "grid_resource_id")
    @NotFound(action=NotFoundAction.IGNORE)
    @JsonIgnore
    private GridResource gridResource;

    @Column(name = "position_x")
    private int posX;

    @Column(name = "position_y")
    private int posY;

    @Column(name = "grid_index")
    private int gridIndex;

    @ManyToOne
    @JoinColumn(name  = "mapped_id")
    @JsonIgnore
    Map map;

    public GridResource getGridResource() {
        return gridResource;
    }

    public void setGridResource(GridResource gridResource) {
        this.gridResource = gridResource;
    }

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    public long getGridId() {
        return gridId;
    }

    public void setGridId(long gridId) {
        this.gridId = gridId;
    }

    public Player getOwner() {
        return owner;
    }

    public void setOwner(Player owner) {
        this.owner = owner;
    }

    public GameEntity getGameEntity() {
        return gameEntity;
    }

    public void setGameEntity(GameEntity gameEntity) {
        this.gameEntity = gameEntity;
    }


    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    public int getGridIndex() {
        return gridIndex;
    }

    public void setGridIndex(int gridIndex) {
        this.gridIndex = gridIndex;
    }
}
