package io.muic.ooc.rtsgame.working.service;

import io.muic.ooc.rtsgame.online.model.Room;
import io.muic.ooc.rtsgame.online.repo.RoomRepository;
import io.muic.ooc.rtsgame.working.factory.GameEntityFactory;
import io.muic.ooc.rtsgame.working.factory.MapFactory;
import io.muic.ooc.rtsgame.working.model.GameEntity;
import io.muic.ooc.rtsgame.working.model.Grid;
import io.muic.ooc.rtsgame.working.model.Map;
import io.muic.ooc.rtsgame.working.model.Player;
import io.muic.ooc.rtsgame.working.repo.GridRepository;
import io.muic.ooc.rtsgame.working.repo.MapRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by wit on 3/21/2017 AD.
 */
@Service
public class MapService {
    /**
     * TODO: findByRoomId, createMap, deleteMap
     */

    @Autowired
    MapRepository mapRepository;

    @Autowired
    RoomRepository roomRepository;

    @Autowired
    GridRepository gridRepository;

    public Map findByRoomId(long id){
        Room r = roomRepository.findOne(id);
        return  null;
    }

    public Map findById(long id){
        return  mapRepository.findOne(id);
    }

    public Map createMap(long desiredSkeletonId, Room room) {
        Map map = MapFactory.createMap(desiredSkeletonId);
        List<Grid> grids = new ArrayList<>();


        map.setGrids(grids);
        map.setRoom(room);
        map.setMapId(room.getId());
        mapRepository.save(map);

        for (int y = 0; y < map.getNumberOfRows(); y++){
            for (int x = 0; x < map.getNumberOfColumns(); x++){
                Grid grid = new Grid();

                grid.setGridIndex(y*map.getNumberOfColumns() + x);
                grid.setPosX(x);
                grid.setPosY(y);
                grid.setMap(map);
                grids.add(grid);

            }
        }

        for (Grid grid: grids){
            gridRepository.save(grid);
        }

        map.setGrids(grids);
        mapRepository.save(map);

        return map;
    }

    public void saveMap(Map map){
        mapRepository.save(map);
    }

    public Map createMap(String mapName, Room room) {
        return createMap(0,room);
    }


    public void deleteMap(long mapId) {
        mapRepository.delete(mapId);
    }
}
