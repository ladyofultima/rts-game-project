package io.muic.ooc.rtsgame.working.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * Created by wit on 3/14/2017 AD.
 */
@Entity
public class PlayerResource {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "player_resource_id")
    long playerResourceId;

    @ManyToOne
    @JoinColumn(name  = "player_id")
    @JsonIgnore
    private Player player;

    @ManyToOne
    @JoinColumn(name  = "resource_id")
    @JsonIgnore
    private Resource resource;

    @Column(name = "value")
    private int value;

    public Player getPlayer() {
        return player;
    }

    public Resource getResource() {
        return resource;
    }

    public int getValue() {
        return value;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
