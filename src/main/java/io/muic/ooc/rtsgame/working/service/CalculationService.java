package io.muic.ooc.rtsgame.working.service;

import io.muic.ooc.rtsgame.working.model.GameEntity;
import org.springframework.stereotype.Service;

/**
 * Created by Trung on 3/24/2017.
 */
@Service
public class CalculationService {

    /**
     * Calculate target's hp after attack
     * @param attacker attacker
     * @param target target
     * @return target's hp post-attack
     */
    public void calculateAttack(GameEntity attacker, GameEntity target){

        int targetHP = target.getCurrentHealth();
        int attackerDmg = attacker.getAttack();

        target.setCurrentHealth(targetHP - attackerDmg);
    }

    /**
     * Set a new ActionTimeStamp for an entity that has just done an action
     * @param entity entity in question
     */
    public void calculateCooldownTime(GameEntity entity){

        double currentTime = System.nanoTime();
        double entityActionCooldownTime = entity.getActionCooldownTime();

        entity.setLastActionTimeStamp(currentTime + entityActionCooldownTime);
    }

    /**
     * Add src's stats to dest (DEST HERE IS EXPECTED TO BE THE RESULTING ENTITY AFTER MERGE)
     * @param src src entity
     * @param dest dest entity
     */
    public void setStatPostMerge(GameEntity src, GameEntity dest){

        int srcMaxHealth = src.getMaxHealth();
        int srcCurrentHealth = src.getCurrentHealth();
        int srcAttackDamage = src.getAttack();

        dest.setMaxHealth(dest.getMaxHealth() + srcMaxHealth);
        dest.setCurrentHealth(dest.getCurrentHealth() + srcCurrentHealth);
        dest.setAttack(dest.getAttack() + srcAttackDamage);
    }
}
