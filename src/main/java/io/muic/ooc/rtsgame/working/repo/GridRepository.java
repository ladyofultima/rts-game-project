package io.muic.ooc.rtsgame.working.repo;

import io.muic.ooc.rtsgame.working.model.Grid;
import io.muic.ooc.rtsgame.working.model.Map;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by wit on 3/16/2017 AD.
 */
@Repository
public interface GridRepository extends CrudRepository<Grid, Long> {
    Grid findByMapAndPosXAndPosY(Map map, int posX, int posY);
}
