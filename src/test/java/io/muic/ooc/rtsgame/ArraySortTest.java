package io.muic.ooc.rtsgame;

import io.muic.ooc.rtsgame.working.factory.MapFactory;
import io.muic.ooc.rtsgame.working.model.Grid;
import io.muic.ooc.rtsgame.working.model.Map;
import io.muic.ooc.rtsgame.working.service.GridIndexSortService;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trung on 4/5/2017.
 */
public class ArraySortTest {

    /**
     * As the GridIndexSortService uses the same algorithm as the TimeStampSortService
     * is can be safely assumed that if one works, the other one should too
     * @throws Exception
     */
    @Test
    public void main() throws Exception {

        GridIndexSortService gridIndexSortService = new GridIndexSortService();
        Map map = MapFactory.createMap(1);

        List<Grid> grids = new ArrayList<>();

        grids.add(createGridMockUp(map, 2, 7));
        grids.add(createGridMockUp(map, 1, 9));
        grids.add(createGridMockUp(map, 14, 1));
        grids.add(createGridMockUp(map, 2, 0));
        grids.add(createGridMockUp(map, 5, 1));

        // Test with unsorted list
        Assert.assertEquals(isSorted(grids), false);

        // Test gridIndexSortService on same list
        gridIndexSortService.sortArray(grids);
        Assert.assertEquals(isSorted(grids), true);
    }

    public Grid createGridMockUp(Map map, int x, int y){

        int c = map.getNumberOfColumns();
        int r = map.getNumberOfRows();

        if (x > c || y > r){ return  null; }

        int inGameId = c * y + x;

        Grid grid = new Grid();
        grid.setPosX(x);
        grid.setPosY(y);
        grid.setGridIndex(inGameId);

        return grid;
    }

    public boolean isSorted(List<Grid> grids){

        int currentIndex = 0;

        for (Grid grid: grids){
            if (grid.getGridIndex() >= currentIndex){ currentIndex = grid.getGridIndex(); }
            else { return false; }
        }

        return true;
    }
}
